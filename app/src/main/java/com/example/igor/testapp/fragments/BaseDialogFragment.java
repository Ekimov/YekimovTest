package com.example.igor.testapp.fragments;


import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;

public class BaseDialogFragment extends DialogFragment{

        public void onResume() {
            Window window = getDialog().getWindow();
            Point size = new Point();
            Display display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            window.setLayout((int) (size.x * 0.75), (int) (size.y * 0.5));
            window.setGravity(Gravity.CENTER);
            super.onResume();
        }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
