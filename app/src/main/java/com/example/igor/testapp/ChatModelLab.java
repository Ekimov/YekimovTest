package com.example.igor.testapp;


import com.example.igor.testapp.models.ChatModel;

import java.util.ArrayList;

public class ChatModelLab {
    private static ChatModelLab chatModelLab;
    private ArrayList<ChatModel> chatModels;
    private ArrayList<ChatModel> archiveChatModels = new ArrayList<>();
    private ChatModelLab(){

    }
    public static ChatModelLab getInstance() {
        if (chatModelLab == null) {
            chatModelLab = new ChatModelLab();
        }
        return chatModelLab;
    }

    public ArrayList<ChatModel> getChatModels() {
        return chatModels;
    }

    public void setChatModels(ArrayList<ChatModel> chatModels) {
        this.chatModels = chatModels;
    }

    public void addChat(ChatModel newChatModel){
        chatModels.add(newChatModel);
    }
    public void remove(ChatModel newChatModel){
        chatModels.remove(newChatModel);
    }
    public void remove(int position){
        chatModels.remove(position);
    }

    public ArrayList<ChatModel> getArchiveChatModels() {
        return archiveChatModels;
    }

    public void setArchiveChatModels(ArrayList<ChatModel> archiveChatModels) {

        this.archiveChatModels = archiveChatModels;
    }
    public void addArchiveChat(ChatModel archiveChat){
        archiveChatModels.add(archiveChat);
    }
}
