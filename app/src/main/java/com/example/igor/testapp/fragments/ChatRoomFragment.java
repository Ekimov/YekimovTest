package com.example.igor.testapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.adapters.ChatRoomAdapter;
import com.example.igor.testapp.models.Message;
import com.example.igor.testapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


public class ChatRoomFragment extends Fragment implements View.OnClickListener {

    private RecyclerView rvMessages;
    private ChatRoomAdapter mAdapter;
    private Button btnSend;
    private EditText etMessage;
    private int position;

    public static ChatRoomFragment newInstance(int position){
        ChatRoomFragment fragment =new ChatRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        position =  getArguments().getInt("position");
        readingMessage(position);
        View view = inflater.inflate(R.layout.chat_room_layout, container, false);
        initView(view);
        mAdapter = new ChatRoomAdapter(ChatModelLab.getInstance().getChatModels().get(position).getMessages());
        rvMessages.setAdapter(mAdapter);
        rvMessages.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMessages.setHasFixedSize(true);
        return view;
    }

    private void readingMessage(int position) {
        for (Message message : ChatModelLab.getInstance().getChatModels().get(position).getMessages()) {
            message.setReading(true);
        }
    }

    private void initView(View view) {
        btnSend = (Button) view.findViewById(R.id.btnSend);
        etMessage = (EditText) view.findViewById(R.id.etMessage);
        rvMessages = (RecyclerView) view.findViewById(R.id.rvMessages);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSend:
                Message message = new Message(etMessage.getText().toString(), Utils.getCurrentTime(),true,true);
                ChatModelLab.getInstance().getChatModels().get(position).getMessages().add(message);
                mAdapter.notifyDataSetChanged();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                etMessage.getText().clear();
                break;
        }
    }
}
