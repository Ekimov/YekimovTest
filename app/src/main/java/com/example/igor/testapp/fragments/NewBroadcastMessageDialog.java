package com.example.igor.testapp.fragments;


import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.models.ChatModel;
import com.example.igor.testapp.models.Message;
import com.example.igor.testapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;

public class NewBroadcastMessageDialog extends BaseDialogFragment{


    private Button btnSendMessage;
    private EditText etMessage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.broadcast_message_layout, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        btnSendMessage = (Button) view.findViewById(R.id.btnCreate);
        etMessage = (EditText) view.findViewById(R.id.etNameChat);
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message newMessage = new Message(etMessage.getText().toString(), Utils.getCurrentTime(),true,false);
                sendAllChatMessage(newMessage);
                EventBus.getDefault().post(newMessage);
                getDialog().cancel();
            }
        });
    }

    private void sendAllChatMessage(Message newMessage) {
        for(ChatModel chatModel: ChatModelLab.getInstance().getChatModels()){
            chatModel.getMessages().add(newMessage);
        }
    }

}
