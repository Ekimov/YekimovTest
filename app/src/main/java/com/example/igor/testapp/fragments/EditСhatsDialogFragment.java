package com.example.igor.testapp.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.models.ChatModel;
import com.example.igor.testapp.models.Message;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

public class EditСhatsDialogFragment extends BaseDialogFragment implements View.OnClickListener {

    private Spinner spChats;
    private SimpleAdapter spChatsAdapter;
    private Button btnDelete;
    private Button btnClearChat;
    private Button btnAddToArchive;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_chats_layout, container, false);
        initView(view);
        setChatsSpinner();
        return view;
    }

    private void setChatsSpinner() {

        ArrayList<HashMap<String, String>> listChats = new ArrayList<>();
        for (int i = 0; i < ChatModelLab.getInstance().getChatModels().size(); i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("name", ChatModelLab.getInstance().getChatModels().get(i).getName());
            listChats.add(map);
        }

        spChatsAdapter = new SimpleAdapter(getActivity(), listChats, R.layout.chats_spinner_item_adapter, new String[]{"name"}, new int[]{R.id.name});
        spChatsAdapter.setDropDownViewResource(R.layout.chats_spinner_dropdown_item);
        spChats.setAdapter(spChatsAdapter);
    }


    private void initView(View view) {
        spChats = (Spinner) view.findViewById(R.id.spChats);
        btnAddToArchive = (Button) view.findViewById(R.id.btnArchive);
        btnClearChat = (Button) view.findViewById(R.id.btnClear);
        btnDelete = (Button) view.findViewById(R.id.btnDelete);
        btnAddToArchive.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnClearChat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ChatModel selectedChat = ChatModelLab.getInstance().getChatModels().get(spChats.getSelectedItemPosition());
        Message defaultBusMessage = new Message(null,null,true,true);
        switch (view.getId()) {
            case R.id.btnArchive:
                ChatModelLab.getInstance().addArchiveChat(selectedChat);
                Toast.makeText(getActivity(),"The chat \""+selectedChat.getName()+"\" has been successfully added to the archive",Toast.LENGTH_LONG).show();
                break;
            case R.id.btnClear:
                selectedChat.getMessages().clear();
                Toast.makeText(getActivity(),"All chat messages were deleted successfully from chat \""+selectedChat.getName()+"\"",Toast.LENGTH_LONG).show();
                break;
            case R.id.btnDelete:
                ChatModelLab.getInstance().getChatModels().remove(selectedChat);
                spChatsAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(),"Chat \""+selectedChat.getName()+"\" has been removed",Toast.LENGTH_LONG).show();
                break;
        }
        EventBus.getDefault().post(defaultBusMessage);
        getDialog().cancel();
    }
}
