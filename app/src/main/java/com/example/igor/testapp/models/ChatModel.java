package com.example.igor.testapp.models;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;


public class ChatModel {
    private final String name;
    private final Drawable imgLogoChats;
    private ArrayList<User> user = new ArrayList<>();
    private final ArrayList<Message> messages;

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public ChatModel(String name,Drawable imgLogoChats, ArrayList<Message> messages) {
        this.name = name;
        this.imgLogoChats = imgLogoChats;
        this.messages = messages;
    }

    public Drawable getImgLogoChats() {
        return imgLogoChats;
    }

    public int getUnreadMessageCount() {
        int unreadMessageCount=0;
        for(Message message:messages){
           if(!message.isReading()) unreadMessageCount+=1;
        }
        return unreadMessageCount;
    }
    public String getName() {

        return name;
    }
}
