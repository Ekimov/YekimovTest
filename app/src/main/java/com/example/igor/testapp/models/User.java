package com.example.igor.testapp.models;


class User {
    private String name, phone, email;
    private int age;

    public User(String name,String phone, int age) {
        this.name = name;
        this.phone =phone;
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {

        return phone;
    }

    public String getName() {

        return name;
    }

    public int getAge() {
        return age;
    }
}
