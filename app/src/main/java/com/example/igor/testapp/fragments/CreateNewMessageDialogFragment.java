package com.example.igor.testapp.fragments;

import android.app.Dialog;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.models.Message;
import com.example.igor.testapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;


public class CreateNewMessageDialogFragment extends BaseDialogFragment {


    private Button btnSendMessage;
    private EditText etMessage;
    private Spinner spChats;
    private SimpleAdapter spChatsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_new_chat_layout, container, false);
        initView(view);
        setChatsSpinner();
        return view;
    }

    private void setChatsSpinner() {
        ArrayList<HashMap<String, String>> listChats = new ArrayList<>();
        for (int i = 0; i < ChatModelLab.getInstance().getChatModels().size(); i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("name", ChatModelLab.getInstance().getChatModels().get(i).getName());
            listChats.add(map);
        }

        spChatsAdapter = new SimpleAdapter(getActivity(), listChats, R.layout.chats_spinner_item_adapter, new String[]{"name"}, new int[]{R.id.name});
        spChatsAdapter.setDropDownViewResource(R.layout.chats_spinner_dropdown_item);
        spChats.setAdapter(spChatsAdapter);
    }

    private void initView(View view) {
        btnSendMessage = (Button) view.findViewById(R.id.btnCreate);
        spChats = (Spinner) view.findViewById(R.id.spChats);
        etMessage = (EditText) view.findViewById(R.id.etNameChat);
        final Drawable todoImage = getActivity().getResources().getDrawable(R.drawable.ic_l8);
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message newMessage = new Message(etMessage.getText().toString(), Utils.getCurrentTime(),true,false);
                ChatModelLab.getInstance().getChatModels().get(spChats.getSelectedItemPosition()).getMessages().add(newMessage);
                EventBus.getDefault().post(newMessage);
                getDialog().cancel();
            }
        });
    }

}
