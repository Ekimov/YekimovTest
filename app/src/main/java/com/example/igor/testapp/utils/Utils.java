package com.example.igor.testapp.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String getCurrentTime(){
        Date date =new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
        return format.format(date);
    }


}
