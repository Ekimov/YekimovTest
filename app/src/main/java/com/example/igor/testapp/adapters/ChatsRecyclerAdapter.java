package com.example.igor.testapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igor.testapp.R;
import com.example.igor.testapp.interfaces.ItemChatsClickListener;
import com.example.igor.testapp.models.ChatModel;

import java.util.ArrayList;
import java.util.List;

public class ChatsRecyclerAdapter extends RecyclerView.Adapter<ChatsRecyclerAdapter.ViewHolder> {
    private ArrayList<ChatModel> chatModels;
    private ItemChatsClickListener chatsClickListener;
    public ChatsRecyclerAdapter(ArrayList<ChatModel> chatModels,ItemChatsClickListener chatsClickListener) {
        this.chatModels = chatModels;
        this.chatsClickListener = chatsClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
         ChatModel chatModel = chatModels.get(position);
         holder.tvName.setText(chatModel.getName());
        if(!chatModel.getMessages().isEmpty()) {
            holder.tvMessage.setText(chatModel.getMessages().get(chatModel.getMessages().size() - 1).getMessageText());
            holder.tvTime.setText(chatModel.getMessages().get(chatModel.getMessages().size() - 1).getTimeMessage());
        }
         holder.tvUnreadMessageCount.setText(""+chatModel.getUnreadMessageCount());
         holder.tvUnreadMessageCount.setVisibility(chatModel.getUnreadMessageCount()==0 ? View.INVISIBLE : View.VISIBLE);
         holder.imLogoChats.setImageDrawable(chatModel.getImgLogoChats());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatsClickListener.OnItemChatClick(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return chatModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvMessage,tvTime, tvUnreadMessageCount;
        ImageView imLogoChats;
        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvUnreadMessageCount = (TextView) itemView.findViewById(R.id.tvUnreadMessageCount);
            imLogoChats = (ImageView) itemView.findViewById(R.id.ivLogo);
        }
    }

    public ArrayList<ChatModel> getChatModels() {
        return chatModels;
    }

    public void addChat(ChatModel chat){
        chatModels.add(chat);
        notifyDataSetChanged();
    }
    public void setFilter(List<ChatModel> chats) {
        chatModels = new ArrayList<>();
        chatModels.addAll(chats);
        notifyDataSetChanged();
    }
}
