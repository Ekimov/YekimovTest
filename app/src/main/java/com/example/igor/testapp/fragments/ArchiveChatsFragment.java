package com.example.igor.testapp.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.adapters.ChatsRecyclerAdapter;
import com.example.igor.testapp.interfaces.ItemChatsClickListener;

public class ArchiveChatsFragment extends Fragment implements ItemChatsClickListener {

    private RecyclerView rvChats;
    private ChatsRecyclerAdapter chatsRecyclerAdapter;
    private ChatsListFragment.OnShowFragmentListener showFragmentListener;
    private TextView tvEmpthyArchive;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        showFragmentListener = (ChatsListFragment.OnShowFragmentListener) context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.archive_chats_layout, container, false);
        rvChats = (RecyclerView) view.findViewById(R.id.rvChats);
        tvEmpthyArchive = (TextView) view.findViewById(R.id.tvEmptyArchive);
        if (ChatModelLab.getInstance().getArchiveChatModels().isEmpty()){
            rvChats.setVisibility(View.GONE);
            tvEmpthyArchive.setVisibility(View.VISIBLE);
        }
        else {
            chatsRecyclerAdapter = new ChatsRecyclerAdapter(ChatModelLab.getInstance().getArchiveChatModels(), this);
            rvChats.setAdapter(chatsRecyclerAdapter);
            rvChats.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        return view;
    }

    @Override
    public void OnItemChatClick(int position) {
        showFragmentListener.onShowChatRoomFragment(position);
    }
}
