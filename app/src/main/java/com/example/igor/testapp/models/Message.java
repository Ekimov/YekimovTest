package com.example.igor.testapp.models;

import java.io.Serializable;


public class Message implements Serializable {
    private String messageText, timeMessage;
    private boolean isSelf;
    private boolean isReading;

    public Message(String messageText, String timeMessage, boolean isSelf, boolean isReading) {
        this.messageText = messageText;
        this.timeMessage = timeMessage;
        this.isSelf=isSelf;
        this.isReading = isReading;
    }

    public String getMessageText() {
        return messageText;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public boolean isReading() {
        return isReading;
    }

    public String getTimeMessage() {

        return timeMessage;
    }

    public void setReading(boolean reading) {
        isReading = reading;
    }
}
