package com.example.igor.testapp.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.igor.testapp.R;
import com.example.igor.testapp.adapters.PageFragmentAdapter;

public class ChatsPagerFragment extends Fragment implements View.OnClickListener {

    private ViewPager pagerChats;
    private Drawable[] imagesActive;
    private Drawable[] imagesNoneActive;
    private LinearLayout llProfile,llPhoneContacts, llChats, llFavourite, llMore;
    private ImageView ivProfile, ivPhoneContacts, ivChats, ivFavourite, ivMore;
    private ImageView [] imageTabViews;
    private PageFragmentAdapter pageAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imagesActive=new Drawable[]{getResources().getDrawable(R.drawable.ic_profile_act),
                getResources().getDrawable(R.drawable.ic_phone_act),
                getResources().getDrawable(R.drawable.ic_chat_act),
                getResources().getDrawable(R.drawable.ic_favourite_act),
                getResources().getDrawable(R.drawable.ic_action_act)};

        imagesNoneActive=new Drawable[]{getResources().getDrawable(R.drawable.ic_profile),
                getResources().getDrawable(R.drawable.ic_phone),
                getResources().getDrawable(R.drawable.ic_chat),
                getResources().getDrawable(R.drawable.ic_favourite),
                getResources().getDrawable(R.drawable.ic_action)};
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chats_pager_fragment, container, false);
        initView(view);
        initBottomTabs(view);
        setupViewPager();
        pagerChats.setCurrentItem(2);
        changeTabs(2);
        return view;
    }

    private void setupViewPager() {
        pageAdapter = new PageFragmentAdapter(getChildFragmentManager(),getActivity());
        pagerChats.setAdapter(pageAdapter);
        pagerChats.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initBottomTabs(View view) {
        llProfile= (LinearLayout)view.findViewById(R.id.llProfile);
        llPhoneContacts= (LinearLayout)view.findViewById(R.id.llPhone);
        llChats= (LinearLayout)view.findViewById(R.id.llChats);
        llFavourite= (LinearLayout)view.findViewById(R.id.llFavourite);
        llMore= (LinearLayout)view.findViewById(R.id.llMore);
        llProfile.setOnClickListener(this);
        llPhoneContacts.setOnClickListener(this);
        llChats.setOnClickListener(this);
        llFavourite.setOnClickListener(this);
        llMore.setOnClickListener(this);

        ivProfile= (ImageView)view.findViewById(R.id.ivProfile);
        ivPhoneContacts= (ImageView)view.findViewById(R.id.ivPhone);
        ivChats= (ImageView)view.findViewById(R.id.ivChats);
        ivFavourite= (ImageView)view.findViewById(R.id.ivFavourite);
        ivMore= (ImageView)view.findViewById(R.id.ivMore);
        imageTabViews =new ImageView[]{ivProfile,ivPhoneContacts,ivChats,ivFavourite,ivMore};
    }

    private void initView(View view) {
        pagerChats = (ViewPager) view.findViewById(R.id.chats_pager);
    }

    private void changeTabs(int position){
        for(int i = 0; i< imageTabViews.length; i++){
            imageTabViews[i].setImageDrawable(imagesNoneActive[i]);
        }
        imageTabViews[position].setImageDrawable(imagesActive[position]);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.llProfile:
                pagerChats.setCurrentItem(0);
                break;
            case R.id.llPhone:
                pagerChats.setCurrentItem(1);
                break;
            case R.id.llChats:
                pagerChats.setCurrentItem(2);
                break;
            case R.id.llFavourite:
                pagerChats.setCurrentItem(3);
                break;
            case R.id.llMore:
                pagerChats.setCurrentItem(4);
                break;
        }
    }
}
