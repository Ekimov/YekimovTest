package com.example.igor.testapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.igor.testapp.R;
import com.example.igor.testapp.models.Message;

import java.util.ArrayList;


public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.ViewHolder> {

    private ArrayList<Message> messages;

    public ChatRoomAdapter(ArrayList<Message> messages) {
        this.messages = messages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_room, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = messages.get(position);
        if (message.isSelf()) {
            holder.message.setVisibility(View.GONE);
            holder.timestamp.setVisibility(View.GONE);
            holder.messageSelf.setVisibility(View.VISIBLE);
            holder.timestampSelf.setVisibility(View.VISIBLE);
            holder.messageSelf.setText(message.getMessageText());
            holder.timestampSelf.setText(message.getTimeMessage());
            return;
        }
        holder.message.setVisibility(View.VISIBLE);
        holder.timestamp.setVisibility(View.VISIBLE);
        holder.messageSelf.setVisibility(View.GONE);
        holder.timestampSelf.setVisibility(View.GONE);
        holder.message.setText(message.getMessageText());
        holder.timestamp.setText(message.getTimeMessage());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message,messageSelf, timestamp, timestampSelf;

        public ViewHolder(View view) {
            super(view);
            message = (TextView) view.findViewById(R.id.message);
            messageSelf = (TextView) view.findViewById(R.id.messageSelf);
            timestamp = (TextView) view.findViewById(R.id.timestamp);
            timestampSelf = (TextView) view.findViewById(R.id.timestampSelf);

        }
    }

}
