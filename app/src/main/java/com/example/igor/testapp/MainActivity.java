package com.example.igor.testapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.igor.testapp.fragments.ChatRoomFragment;
import com.example.igor.testapp.fragments.ChatsListFragment;
import com.example.igor.testapp.fragments.ChatsPagerFragment;
import com.example.igor.testapp.models.Message;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ChatsListFragment.OnShowFragmentListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragCont);
        if (fragment == null) {
            showPagerFragment();
        }
    }

    private void showPagerFragment() {
        ChatsPagerFragment chatsPagerFragment = new ChatsPagerFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragCont,chatsPagerFragment);
        ft.commit();
    }

    @Override
    public void onShowChatRoomFragment(int position) {
        ChatRoomFragment chatRoomFragment = ChatRoomFragment.newInstance(position);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragCont,chatRoomFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
