package com.example.igor.testapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.igor.testapp.fragments.ChatsListFragment;
import com.example.igor.testapp.fragments.FavouriteChatsFragment;
import com.example.igor.testapp.fragments.MoreOptionsFragment;
import com.example.igor.testapp.fragments.PhoneFragment;
import com.example.igor.testapp.fragments.ProfileFragment;


public class PageFragmentAdapter extends FragmentStatePagerAdapter {
    private Context context;
    public PageFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return Fragment.instantiate(context, ProfileFragment.class.getName());
            case 1:
                return Fragment.instantiate(context, PhoneFragment.class.getName());
            case 2:
                return Fragment.instantiate(context, ChatsListFragment.class.getName());
            case 3:
                return Fragment.instantiate(context, FavouriteChatsFragment.class.getName());
            case 4:
                return Fragment.instantiate(context, MoreOptionsFragment.class.getName());
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
