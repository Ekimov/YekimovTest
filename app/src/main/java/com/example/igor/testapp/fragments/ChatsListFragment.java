package com.example.igor.testapp.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.igor.testapp.ChatModelLab;
import com.example.igor.testapp.R;
import com.example.igor.testapp.adapters.ChatsRecyclerAdapter;
import com.example.igor.testapp.interfaces.ItemChatsClickListener;
import com.example.igor.testapp.models.ChatModel;
import com.example.igor.testapp.models.Message;
import com.example.igor.testapp.utils.Utils;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class ChatsListFragment extends Fragment implements View.OnClickListener,ItemChatsClickListener {
    public  interface OnShowFragmentListener{
        void onShowChatRoomFragment(int position);
    }
    private ImageView imCreateNew;
    private RecyclerView rvChats;
    private TextView tvArchiveChatsCount, tvBroadcastMessage;
    private ChatsRecyclerAdapter chatsRecyclerAdapter;
    private Drawable[] tmpLogoImages;
    private ExpandableRelativeLayout searchContainer;
    private OnShowFragmentListener showFragmentListener;
    private boolean isExpandSearch;
    private LinearLayout llArchiveChats,llEditChat;
    private SearchView searchView;
    private int archiveChatsCount;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        showFragmentListener = (OnShowFragmentListener) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chats_layout, container, false);
        initView(view);
        if (ChatModelLab.getInstance().getChatModels() == null) {
            setData();
        }
        chatsRecyclerAdapter = new ChatsRecyclerAdapter(ChatModelLab.getInstance().getChatModels(), this);
        rvChats.setAdapter(chatsRecyclerAdapter);
        tvArchiveChatsCount.setText("("+archiveChatsCount+")");
        rvChats.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        rvChats.addItemDecoration(itemDecoration);
        rvChats.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isExpandSearch = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(isExpandSearch) {
                    if (dy > 0) {
                        searchContainer.expand();
                    } else if(dy<0) {
                        searchContainer.collapse();
                    }
                }
            }
        });
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener
                (new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return true;
                    }
                    @Override
                    public boolean onQueryTextChange(String s) {
                        ArrayList<ChatModel>filteredModelList = filter(ChatModelLab.getInstance().getChatModels(), s);
                        chatsRecyclerAdapter.setFilter(filteredModelList);
                        return true;
                    }
                });
        return view;
    }
    private ArrayList<ChatModel> filter(ArrayList<ChatModel> chatModels, String query) {
        query = query.toLowerCase();

        final ArrayList<ChatModel>filteredModelList = new ArrayList<>();
        for (ChatModel chatModel : chatModels) {
            final String text = chatModel.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(chatModel);
            }
        }
        return filteredModelList;
    }

    @Override
    public void onPause() {
        super.onPause();
       isExpandSearch = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void setData() {
        tmpLogoImages = new Drawable[]{getResources().getDrawable(R.drawable.ic_l),
                getResources().getDrawable(R.drawable.ic_7),
                getResources().getDrawable(R.drawable.ic_l8),
                getResources().getDrawable(R.drawable.ic_l2),
                getResources().getDrawable(R.drawable.ic_l4),
                getResources().getDrawable(R.drawable.ic_7),
                getResources().getDrawable(R.drawable.ic_l8),
                getResources().getDrawable(R.drawable.ic_l2),
                getResources().getDrawable(R.drawable.ic_l),
                getResources().getDrawable(R.drawable.ic_l4),
                getResources().getDrawable(R.drawable.ic_l),
                getResources().getDrawable(R.drawable.ic_7),
                getResources().getDrawable(R.drawable.ic_l8),
                getResources().getDrawable(R.drawable.ic_l2),
                getResources().getDrawable(R.drawable.ic_l4),
                getResources().getDrawable(R.drawable.ic_7),
                getResources().getDrawable(R.drawable.ic_l8),
                getResources().getDrawable(R.drawable.ic_l2),
                getResources().getDrawable(R.drawable.ic_l),
                getResources().getDrawable(R.drawable.ic_l4)
        };

       ArrayList<ChatModel> chatModels = new ArrayList<>();
        for(int i=0; i<10; i++){
            int position = i % 4;
            switch (position) {
                case 0:
                    chatModels.add(new ChatModel("Vanessa",tmpLogoImages[i],getTodoMessages()));
                    break;
                case 1:
                    chatModels.add(new ChatModel("Joicy lau",tmpLogoImages[i],getTodoMessages()));
                    break;
                case 2:
                    chatModels.add(new ChatModel("Mom",tmpLogoImages[i],getTodoMessages()));
                    break;
                case 3:
                    chatModels.add(new ChatModel("Confer",tmpLogoImages[i],getTodoMessages()));
                    break;
            }
        }
        ChatModelLab.getInstance().setChatModels(chatModels);

    }

    private ArrayList<Message> getTodoMessages() {
        ArrayList message= new ArrayList();
        for(int i=0; i<10; i++){
            message.add(new Message("Message "+i, Utils.getCurrentTime(), i % 4 == 0, i % 4 != 0));
        }
        return message;
    }

    private void initView(View view) {
        searchContainer = (ExpandableRelativeLayout) view.findViewById(R.id.searchContainer);
        rvChats = (RecyclerView) view.findViewById(R.id.rvChats);
        tvArchiveChatsCount = (TextView) view.findViewById(R.id.tvCounArchiveChats);
        llEditChat = (LinearLayout) view.findViewById(R.id.llEdit);
        searchView = (SearchView) view.findViewById(R.id.svChats);
        imCreateNew = (ImageView) view.findViewById(R.id.imCreateNew);
        llArchiveChats = (LinearLayout) view.findViewById(R.id.llArchiveChats);
        tvBroadcastMessage = (TextView) view.findViewById(R.id.tvBroadcast);
        llArchiveChats.setOnClickListener(this);
        tvBroadcastMessage.setOnClickListener(this);
        imCreateNew.setOnClickListener(this);
        llEditChat.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
          switch (view.getId()){
              case R.id.imCreateNew:
                  new CreateNewMessageDialogFragment().show(getChildFragmentManager(),"");
                  searchContainer.setClosePosition(0);
                  break;
              case R.id.llEdit:
                  new EditСhatsDialogFragment().show(getChildFragmentManager(),"");
                  break;
              case R.id.tvBroadcast:
                  new NewBroadcastMessageDialog().show(getChildFragmentManager(),"");
                  break;
              case R.id.llArchiveChats:
                  getActivity().getSupportFragmentManager().beginTransaction()
                          .replace(R.id.fragCont,new ArchiveChatsFragment())
                          .addToBackStack(null)
                          .commit();
                  break;
          }

    }
    @Subscribe
    public void onEvent(Message message){
        chatsRecyclerAdapter.notifyDataSetChanged();
        archiveChatsCount = ChatModelLab.getInstance().getArchiveChatModels().size();
        tvArchiveChatsCount.setText("("+archiveChatsCount+")");
    }
    @Override
    public void OnItemChatClick(int position) {
        showFragmentListener.onShowChatRoomFragment(position);
    }

}
